//
//  Services.swift
//  AppTeste
//
//  Created by marcel.soares on 14/10/20.
//

import Foundation
import Alamofire

protocol ServiceRequests {
    func loadEvents(completion: @escaping (String?, [Event]?) -> Void)
    func loadEventDetail(eventId: String, completion: @escaping (String?, Event?) -> Void)
    func checkin(parameters: [String: Any], completion: @escaping (String?, CheckInResponse?) -> Void)
}

class ServiceManager {
    
    // MARK: - Config
    static let BASE_URL = "http://5f5a8f24d44d640016169133.mockapi.io/api/"
    static let HEADER : HTTPHeaders = [
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Accept": "application/json, text/plain, */*"]
    var alamoFireManager : Alamofire.Session?
    
    // MARK: - Endpoints
    static let ENDPOINT_EVENTS = "events"
    static let ENDPOINT_EVENT_DETAIL = "events/"
    static let ENDPOINT_CHECKIN = "checkin"
    
    
    func request<T:Decodable>(method: HTTPMethod, parameters: [String: Any]?, endpoint: String, completion: @escaping (String?, T?) -> Void){

        let urlString = ServiceManager.BASE_URL + endpoint

        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        configuration.timeoutIntervalForResource = 5
        alamoFireManager = Alamofire.Session(configuration: configuration)
        
        _ = alamoFireManager?.request(urlString, method: method, parameters: parameters).responseData { response in
            //Need to retain session
            _ = self

            if let error = response.error {
                
                if let errorMessage = error.errorDescription?.split(separator: ":").last {
                    completion("\(errorMessage)", nil)
                    return
                }
                completion(nil, nil)
                return
            }
            
            
            switch response.result {
            case .success:
                
                guard let jsonData = response.data,
                      let response = try? JSONDecoder().decode(T.self, from: jsonData) else {
                    completion(nil, nil)
                    return
                }
                completion(nil, response)
                
            case .failure:
                completion(nil, nil)
            }
        }
        
    }
    

}

extension ServiceManager: ServiceRequests {
    
    func loadEvents(completion: @escaping (String?, [Event]?) -> Void){
        
        request(method: .get, parameters: nil, endpoint:ServiceManager.ENDPOINT_EVENTS, completion: completion)
    }
    
    func loadEventDetail(eventId: String, completion: @escaping (String?, Event?) -> Void){

        request(method: .get, parameters: nil, endpoint:ServiceManager.ENDPOINT_EVENT_DETAIL + eventId, completion: completion)
    }
    
    func checkin(parameters: [String: Any], completion: @escaping (String?, CheckInResponse?) -> Void){
        
        request(method: .post, parameters: parameters, endpoint:ServiceManager.ENDPOINT_CHECKIN) { (errorMessage, checkInResponse:CheckInResponse?) in
            completion(errorMessage, checkInResponse)
        }
    }
}

struct CheckInResponse: Decodable {
    var code : String?
}
