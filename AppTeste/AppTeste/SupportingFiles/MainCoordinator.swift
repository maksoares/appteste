//
//  MainCoordinator.swift
//  AppTeste
//
//  Created by marcel.soares on 22/10/20.
//

import Foundation
import UIKit

public protocol Coordinator: AnyObject {
    var children: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var parent: Coordinator? { get set }
    func start()
}

public extension Coordinator {
    func add(_ child: Coordinator) {
        children.append(child)
        child.parent = self
    }

    func remove(_ child: Coordinator) {
        children.removeAll { $0 === child }
    }
}


class MainCoordinator: Coordinator {
    
    // MARK: Attributes
    public var children = [Coordinator]()
    var navigationController : UINavigationController
    weak public var parent: Coordinator?
    
    // MARK: Initalizers
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController!
    }
    
    func start() {
        let vc = EventsViewController()
        vc.coordinator =  self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func finish() {
        navigationController.popToRootViewController(animated: true)
    }
    
    func eventDetail(eventId: String) {
        let vc = EventDetailViewController(eventId: eventId)
        vc.coordinator =  self
        navigationController.pushViewController(vc, animated: true)
    }

    func checkin(eventId: String) {
        let vc = CheckinViewController(eventId: eventId)
        vc.coordinator =  self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showActivityViewController(viewController: UIViewController, message: String) {
        
        var objs = [Any]()
        objs.append(message)
        if let url = NSURL(string: "appteste://eventDetail/2") {
            objs.append(url)
        }

        let activityViewController = UIActivityViewController(activityItems: objs, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = viewController.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        viewController.present(activityViewController, animated: true, completion: nil)
                
    }
    
    
    func shareEventDetailActivityViewController(viewController: UIViewController, message: String, eventId: String) {
        
        var shareInfo = [Any]()
        shareInfo.append(message)
        
        let str = DeeplinkParser.SCHEME + DeeplinkParser.EVENT_DETAIL + "/" + eventId
        if let url = NSURL(string: str) {
            shareInfo.append(url)
        }

        let activityViewController = UIActivityViewController(activityItems: shareInfo, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = viewController.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        viewController.present(activityViewController, animated: true, completion: nil)
                
    }
    
}
