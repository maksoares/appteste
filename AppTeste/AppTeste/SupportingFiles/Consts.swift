//
//  Consts.swift
//  AppTeste
//
//  Created by marcel.soares on 19/10/20.
//

import Foundation
import UIKit
import CoreGraphics

struct Consts {
    public static let TITLE_FONT = UIFont.boldSystemFont(ofSize: 14)
    public static let DETAIL_TITLE_FONT = UIFont.boldSystemFont(ofSize: 16)
    public static let DETAIL_FONT = UIFont.systemFont(ofSize: 12)
    public static let DETAIL_BOLD_FONT = UIFont.boldSystemFont(ofSize: 12)
}

struct ViewConstraints {
    
    // MARK: - Spacing
    static let LEADING_SPACE: CGFloat = 16.0
    static let TRAILING_SPACE: CGFloat = -16.0
    static let TOP_SPACE: CGFloat = 8.0
    
    static let BOTTOM_SPACE: CGFloat = -8.0
    static let BUTTON_TOP_SPACE: CGFloat = 24.0
    static let BUTTON_HEIGHT: CGFloat = 40.0
    
    // MARK: - Events
    static let TITLE_LABEL_HEIGHT: CGFloat = 30.0
    static let SUB_TITLE_LABEL_HEIGHT: CGFloat = 20.0
    
    // MARK: - Events Detail
    static let IMAGE_HEIGHT: CGFloat = 240.0
    static let COLLECTION_VIEW_HEIGHT: CGFloat = 224.0
    static let COLLECTION_VIEW_ITEM_HEIGHT: CGFloat = 200.0
    static let SCROLL_VIEW_MARGIN: CGFloat = 24.0
    static let CONTENT_WIDTH: CGFloat
        = UIScreen.main.bounds.width - LEADING_SPACE * 2
    static let INFO_WIDTH: CGFloat = 80
    
    // MARK: - Checkin
    static let CHECKIN_TEXT_HEIGHT: CGFloat = 40.0
    
}

struct Spacing {
    //MARK: General
    static let TOP: CGFloat = 8.0
    static let BOTTOM: CGFloat = -8.0
    static let LEFT: CGFloat = 8.0
    static let RIGHT: CGFloat = -8.0
}

