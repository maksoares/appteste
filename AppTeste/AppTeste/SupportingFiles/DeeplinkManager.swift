//
//  DeeplinkManager.swift
//  AppTeste
//
//  Created by marcel.soares on 22/10/20.
//

import Foundation
import UIKit

enum DeeplinkType {
    
    enum Content {
        case root
        case details(id: String)
    }
    
    case events
    case eventDetail(id: String)
    case checkin(id: String)
}


class DeeplinkNavigator {
    static let shared = DeeplinkNavigator()
    private init() { }
    
    func proceedToDeeplink(_ type: DeeplinkType) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let nav = appDelegate.window?.rootViewController as? UINavigationController else {
            return
        }
        
        nav.popToRootViewController(animated: false)
        let topViewController = nav.topViewController as? EventsViewController
        
        switch type {
        case .events:
            topViewController?.coordinator?.finish()
            
        case .eventDetail(id: let id):
            topViewController?.coordinator?.eventDetail(eventId: id)
            
        case .checkin(id: let id):
            topViewController?.coordinator?.checkin(eventId: id)
        }
        
    }
    
}


public class DeepLinkManager {
    init() {}
    private var deeplinkType: DeeplinkType?

    
    @discardableResult
    func handleDeeplink(url: URL) -> Bool {
        deeplinkType = DeeplinkParser.shared.parseDeepLink(url)
        
        if let deeplinkType = deeplinkType {
            DeeplinkNavigator.shared.proceedToDeeplink(deeplinkType)
        }
        
        return deeplinkType != nil
    }
    
    func checkDeepLink() {
        guard let deeplinkType = deeplinkType else {
            return
        }
        
        DeeplinkNavigator.shared.proceedToDeeplink(deeplinkType)
        self.deeplinkType = nil
    }
}



class DeeplinkParser {
    static let shared = DeeplinkParser()
    init() { }
    
    //MARK
    static let SCHEME = "appteste://"
    
    static let EVENTS = "events"
    static let EVENT_DETAIL = "eventDetail"
    static let CHECKIN = "checkin"
    
    func parseDeepLink(_ url: URL) -> DeeplinkType? {
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true), let host = components.host else {
            return nil
        }
        var pathComponents = components.path.components(separatedBy: "/")
        
        pathComponents.removeFirst()
        switch host {
        case DeeplinkParser.EVENTS:
            return DeeplinkType.events
        case DeeplinkParser.EVENT_DETAIL:
            if let requestId = pathComponents.first {
                return DeeplinkType.eventDetail(id: requestId)
            }
        case DeeplinkParser.CHECKIN:
            if let requestId = pathComponents.first {
                return DeeplinkType.checkin(id: requestId)
            }
        default:
            break
        }
        return nil
    }
    
}
