//
//  EventDetailViewController.swift
//  AppTeste
//
//  Created by marcel.soares on 18/10/20.
//

import Foundation
import RxSwift
import RxCocoa
import Nuke

class EventDetailViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Properties
    var coordinator: MainCoordinator?
    var viewModel: EventDetailViewModel = EventDetailViewModel()
    var customView = EventDetailView()

    // MARK: - Private
    private let disposeBag = DisposeBag()
    private var eventId = ""
    
    convenience init(eventId: String) {
        self.init()
        self.eventId = eventId
    }
    
    // MARK: - UIViewController
    override func loadView() {
        view = customView
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupBinds()
        viewModel.loadEventDetail(eventId: eventId)
    }
    
    
    // MARK: - Binds
    private func setupBinds() {
        
        //Event
        viewModel.event.subscribe(onNext: { (event) in
            self.updateView(event: event)
        })
        .disposed(by: disposeBag)
        
        //Image loading
        customView.imageView.rx.isEmpty.subscribe(onNext: { isEmpty in
            self.customView.imageActivityIndicatorView.isHidden = !isEmpty
            isEmpty ? self.customView.imageActivityIndicatorView.startAnimating() : self.customView.imageActivityIndicatorView.stopAnimating()
        }).disposed(by: disposeBag)
        
        
        //Error
        viewModel.error.subscribe({ (messageError) in
            let alertController = UIAlertController.customAlertController(title: NSLocalizedString("Error", comment: ""), message: messageError.element ?? "", tryAgain: true){ tryAgain in
                tryAgain ? (self.viewModel.loadEventDetail(eventId: self.eventId)) : self.coordinator?.finish()
            }
            self.present(alertController, animated: true, completion: nil)
        })
        .disposed(by: disposeBag)
        
        // Loading
        viewModel.loading.subscribe(onNext: { (isLoading) in
            self.customView.activityIndicatorView.isHidden = !isLoading
            isLoading ? self.customView.activityIndicatorView.startAnimating() : self.customView.activityIndicatorView.stopAnimating()
        })
        .disposed(by: disposeBag)
        

        // Collection
        customView.collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        viewModel.people.bind(to: customView.collectionView.rx.items){ (collectionView, row, item) -> UICollectionViewCell in

            let indexPath = IndexPath(row: row, section: 0)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PeopleCollectionViewCell.REUSE_IDENTIFIER, for: indexPath) as! PeopleCollectionViewCell
            
            cell.people = item
            return cell
        }.disposed(by: disposeBag)
        
        
        // Checkin
        customView
            .checkinButton
            .rx.tap
            .subscribe(onNext: { _ in
                self.coordinator?.checkin(eventId: self.eventId)
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Interface
    func configureView() {
        
        //Navigation Bar
        configureNavigationBar(title: NSLocalizedString("Details", comment: ""), buttonSystemType: .action, disposeBag: disposeBag){
            self.coordinator?.shareEventDetailActivityViewController(viewController: self, message: self.viewModel.shareText, eventId: self.eventId)
        }

    }
    
    func updateView(event: Event) {
        //Title
        customView.titleLabel.text = event.title
        customView.imageView.loadImageFromString(url: event.image)
        customView.descriptionValueLabel.text = event.description

        customView.dateValueLabel.text = event.date?.epochToDateStr()
        customView.timeValueLabel.text = event.date?.epochToTimeStr()
        customView.priceValueLabel.text = event.price?.moneyToStr()
        
        self.customView.addressValueLabel.text = ""
        CustomFunctions().getAddressFromLocation(latitude: event.latitude, longitude: event.longitude, completion: { (address) in
            self.customView.addressValueLabel.text = address
        })
        
        if (event.people?.count == 0){
            customView.collectionView.heightConstraint?.constant = 0
            customView.collectionView.layoutIfNeeded()
            
            customView.collectionContentView.heightConstraint?.constant = 0
            customView.collectionContentView.layoutIfNeeded()
            
        } else {
            configureCollectionView()
        }
    }
    
    private func configureCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        let width = (customView.collectionView.frame.size.width - ViewConstraints.SCROLL_VIEW_MARGIN) / 1.5
        
        flowLayout.itemSize = CGSize(width: width, height: ViewConstraints.COLLECTION_VIEW_ITEM_HEIGHT)
        customView.collectionView.setCollectionViewLayout(flowLayout, animated: true)
    }

}

