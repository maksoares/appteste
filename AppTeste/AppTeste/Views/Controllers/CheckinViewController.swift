//
//  CheckinViewController.swift
//  AppTeste
//
//  Created by marcel.soares on 19/10/20.
//

import Foundation
import RxSwift
import RxCocoa

class CheckinViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Properties
    var coordinator: MainCoordinator?
    var viewModel: CheckinViewModel = CheckinViewModel()
    var customView = CheckinView()

    // MARK: - Private
    private let disposeBag = DisposeBag()
    private var eventId = ""
    
    convenience init(eventId: String) {
        self.init()
        self.eventId = eventId
    }
    
    // MARK: - UIViewController
    override func loadView() {
        view = customView
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupBinds()
    }
    
    // MARK: - Binds
    private func setupBinds() {
        
        // Checkin
        customView
            .checkinButton
            .rx.tap
            .subscribe(onNext: { _ in
                self.viewModel.checkin(eventId: self.eventId, name: self.customView.nameTextView.text,
                                       email: self.customView.emailTextView.text)
            }).disposed(by: disposeBag)

        
        //Name
        viewModel.nameError.subscribe({ (message) in
            let alert = UIAlertController.customAlertController(title: NSLocalizedString("Error", comment: ""),
                                                                message: message.element ?? ""){ _ in
                self.customView.nameTextView.becomeFirstResponder()
            }
            self.present(alert, animated: true, completion: nil)
        })
        .disposed(by: disposeBag)
        
        //Email
        viewModel.emailError.subscribe({ (message) in
            let alert = UIAlertController.customAlertController(title: NSLocalizedString("Error", comment: ""),
                                                                message: message.element ?? ""){ _ in
                self.customView.emailTextView.becomeFirstResponder()
            }
            self.present(alert, animated: true, completion: nil)
        })
        .disposed(by: disposeBag)
        
        //Checkin
        viewModel.checkin.subscribe(onNext: { (checkin) in

            if checkin {
                let alert = UIAlertController.customAlertController(
                    title: NSLocalizedString("Alert", comment: ""),
                    message: NSLocalizedString("Successful check-in", comment: "")){ _ in
                    self.coordinator?.finish()
                }
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController.customAlertController(
                    title: NSLocalizedString("Error", comment: ""),
                    message: NSLocalizedString("Failed to check-in", comment: ""), tryAgain: true){ tryAgain in
                    if tryAgain {
                        self.viewModel.checkin(eventId: self.eventId, name: self.customView.nameTextView.text, email: self.customView.emailTextView.text)
                    }
                }
                self.present(alert, animated: true, completion: nil)
            }
        })
        .disposed(by: disposeBag)
        
        // Loading
        viewModel.loading.subscribe(onNext: { (isLoading) in
            self.showActivityIndicator(loading: isLoading, activityIndicatorView: self.customView.activityIndicatorView)
        })
        .disposed(by: disposeBag)

    }

    // MARK: - Interface
    func configureView() {
        //NavigationBar
        navigationItem.title = NSLocalizedString("Checkin", comment: "")
        
        hideKeyboardWhenTappedAround()
        
        customView.nameTextView.becomeFirstResponder()
    }
    
}
