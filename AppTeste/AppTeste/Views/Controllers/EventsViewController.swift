//
//  EventsViewController.swift
//  AppTeste
//
//  Created by marcel.soares on 14/10/20.
//

import UIKit
import RxSwift
import RxCocoa

class EventsViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Properties
    var coordinator: MainCoordinator?
    var viewModel: EventsViewModel = EventsViewModel()
    var customView = EventsView()

    // MARK: - Private
    private let disposeBag = DisposeBag()
    
    
    // MARK: - UIViewController
    override func loadView() {
        view = customView
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupBinds()
        viewModel.loadEvents()
    }
    
    
    // MARK: - Binds
    private func setupBinds() {
        
        //TableView
        customView.tableView.rx.setDelegate(self).disposed(by: disposeBag)
        customView.tableView.register(EventTableViewCell.self, forCellReuseIdentifier: EventTableViewCell.REUSE_IDENTIFIER)
        
        viewModel.events.bind(to: customView.tableView.rx.items(cellIdentifier: EventTableViewCell.REUSE_IDENTIFIER, cellType: EventTableViewCell.self)) {
            (row,event,cell) in
            cell.event = event
        }.disposed(by: disposeBag)
        customView.tableView.rx.modelSelected(Event.self).subscribe(onNext: { event in
            
            self.coordinator?.eventDetail(eventId: event.eventId ?? "")
            
        }).disposed(by: disposeBag)
        
        
        //Error
        viewModel.error.subscribe({ (messageError) in
            let alertController = UIAlertController.customAlertController(title: NSLocalizedString("Error", comment: ""), message: messageError.element ?? "", tryAgain: true){ tryAgain in
                if tryAgain {(self.viewModel.loadEvents())}
            }
            self.present(alertController, animated: true, completion: nil)
        })
        .disposed(by: disposeBag)
        
        // Loading
        viewModel.loading.subscribe(onNext: { (isLoading) in
            self.showActivityIndicator(loading: isLoading, activityIndicatorView: self.customView.activityIndicatorView)
        })
        .disposed(by: disposeBag)

    }

    // MARK: - Interface
    func configureView() {
        
        //Navigation Bar
        configureNavigationBar(title: NSLocalizedString("Events", comment: ""), buttonSystemType: .refresh, disposeBag: disposeBag){
            self.viewModel.loadEvents()
        }
        
        //TableView
        customView.tableView.contentInset = UIEdgeInsets(top: Spacing.TOP, left: 0, bottom: Spacing.BOTTOM, right: 0)
        
    }
    
}
