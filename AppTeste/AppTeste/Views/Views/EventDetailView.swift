//
//  EventDetailView.swift
//  AppTeste
//
//  Created by marcel.soares on 18/10/20.
//

import Foundation
import UIKit

class EventDetailView: UIView {
    
    // MARK: Subviews
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.transform = CGAffineTransform(scaleX: 2, y: 2)
        activityIndicator.tintColor = .gray
        activityIndicator.backgroundColor = UIColor(white: 1, alpha: 1)
        return activityIndicator
    }()
    
    
    var titleContentView: UIView = {
        let view = UIView()
        return view
    }()
    
    let titleLabel: CustomLabel = {
        let label = CustomLabel()
        label.font = Consts.DETAIL_TITLE_FONT
        label.textAlignment = .center
        return label
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .white
        return imageView
    }()
    
    var imageActivityIndicatorView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.transform = CGAffineTransform(scaleX: 1, y: 1)
        activityIndicator.tintColor = .gray
        activityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.1)
        return activityIndicator
    }()
    
    var descriptionContentView: UIView = {
        let view = UIView()
        return view
    }()
    let descriptionValueLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    
    var infoContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    var infoView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.systemGray6
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1.0
        return view
    }()
    
    let addressLabel: CustomLabel = {
        let label = CustomLabel()
        label.text = NSLocalizedString("Address: ", comment: "")
        label.font = Consts.DETAIL_BOLD_FONT
        return label
    }()
    
    let addressValueLabel: CustomLabel = {
        let label = CustomLabel()
        label.textAlignment = .justified
        return label
    }()
    
    let dateLabel: CustomLabel = {
        let label = CustomLabel()
        label.text = NSLocalizedString("Date: ", comment: "")
        label.font = Consts.DETAIL_BOLD_FONT
        return label
    }()
    
    let dateValueLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = CustomLabel()
        label.text = NSLocalizedString("Hour: ", comment: "")
        label.font = Consts.DETAIL_BOLD_FONT
        return label
    }()
    
    let timeValueLabel: UILabel = {
        let label = CustomLabel()
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = CustomLabel()
        label.text = NSLocalizedString("Price: ", comment: "")
        label.font = Consts.DETAIL_BOLD_FONT
        return label
    }()
    
    let priceValueLabel: UILabel = {
        let label = CustomLabel()
        return label
    }()

    var collectionContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let frame = CGRect(x: 0, y: 0, width: ViewConstraints.COLLECTION_VIEW_ITEM_HEIGHT
                           , height: ViewConstraints.COLLECTION_VIEW_ITEM_HEIGHT)
        let collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.register(PeopleCollectionViewCell.self, forCellWithReuseIdentifier: PeopleCollectionViewCell.REUSE_IDENTIFIER)
        return collectionView
    }()
    
    var checkinContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    public var checkinButton: UIButton = {
        let button = UIButton()
        button.setTitle(NSLocalizedString("Checkin", comment: ""), for: .normal)
        button.titleLabel?.font = Consts.DETAIL_BOLD_FONT
        button.setTitleColor(.link, for: .normal)
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.link.cgColor
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 1.0
        return button
    }()
    
    // MARK: Initializers
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    // MARK: Setup Layout
    func setupView() {
        addSubviews()
        setupConstraints()
        backgroundColor = .white
    }
    
    func addSubviews() {
        addSubview(scrollView)
        scrollView.addSubview(contentView)

        contentView.addSubview(titleContentView)
        titleContentView.addSubview(titleLabel)
        
        contentView.addSubview(imageView)
        contentView.addSubview(imageActivityIndicatorView)
        
        contentView.addSubview(descriptionContentView)
        descriptionContentView.addSubview(descriptionValueLabel)
        
        contentView.addSubview(infoContentView)
        infoContentView.addSubview(infoView)
        
        infoView.addSubview(dateLabel)
        infoView.addSubview(dateValueLabel)
        infoView.addSubview(timeLabel)
        infoView.addSubview(timeValueLabel)
        infoView.addSubview(priceLabel)
        infoView.addSubview(priceValueLabel)
        infoView.addSubview(addressLabel)
        infoView.addSubview(addressValueLabel)
        
        contentView.addSubview(collectionContentView)
        collectionContentView.addSubview(collectionView)
        
        contentView.addSubview(checkinContentView)
        checkinContentView.addSubview(checkinButton)
        addSubview(activityIndicatorView)
    }
    
    
    // MARK: Constraints
    func setupConstraints() {
        
        scrollViewConstraints()
        
        //Title
        addContentViewConstraints(view: titleContentView, height: ViewConstraints.SUB_TITLE_LABEL_HEIGHT, topAnchor: contentView.topAnchor, topMargin: ViewConstraints.SCROLL_VIEW_MARGIN)
        addSubViewConstraints(view: titleLabel, contentView: titleContentView)
        
        //Image
        addContentViewConstraints(view: imageView, height: ViewConstraints.IMAGE_HEIGHT, topAnchor: titleContentView.bottomAnchor, topMargin: ViewConstraints.SCROLL_VIEW_MARGIN)
        addContentViewConstraints(view: imageActivityIndicatorView, height: ViewConstraints.IMAGE_HEIGHT, topAnchor: titleContentView.bottomAnchor, topMargin: ViewConstraints.SCROLL_VIEW_MARGIN)

        //Description
        addContentViewConstraints(view: descriptionContentView, height: nil, topAnchor: imageView.bottomAnchor, topMargin: ViewConstraints.SCROLL_VIEW_MARGIN)
        addSubViewConstraints(view: descriptionValueLabel, contentView: descriptionContentView)
        
        //Info
        addContentViewConstraints(view: infoContentView, height: nil, topAnchor: descriptionValueLabel.bottomAnchor, topMargin: ViewConstraints.SCROLL_VIEW_MARGIN)
        addSubViewConstraints(view: infoView, contentView: infoContentView)
        infoViewConstraints()
        
        //CollectionView
        addContentViewConstraints(view: collectionContentView, height: ViewConstraints.COLLECTION_VIEW_HEIGHT, topAnchor: infoView.bottomAnchor)
        collectionViewConstraints()

        //Checkin
        addContentViewConstraints(view: checkinContentView, height: nil, topAnchor: collectionContentView.bottomAnchor, topMargin: ViewConstraints.SCROLL_VIEW_MARGIN)
        addSubViewConstraints(view: checkinButton, contentView: checkinContentView)
        
        //Last
        checkinContentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -ViewConstraints.SCROLL_VIEW_MARGIN).isActive = true
        
        activityIndicatorViewConstraints()
    }
    
    private func scrollViewConstraints(){
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
    }
    

    private func infoViewConstraints(){
        
        var lastView = UIView()
        
        //Date
        lastView = addInfoSubView(infoView: infoView, titleView: dateLabel, valueView: dateValueLabel, topAnchor: infoView.topAnchor)
        
        //Time
        lastView = addInfoSubView(infoView: infoView, titleView: timeLabel, valueView: timeValueLabel, topAnchor: lastView.bottomAnchor)
        
        //Price
        lastView = addInfoSubView(infoView: infoView, titleView: priceLabel, valueView: priceValueLabel, topAnchor: lastView.bottomAnchor)
        
        //Address
        lastView = addInfoSubView(infoView: infoView, titleView: addressLabel, valueView: addressValueLabel, topAnchor: lastView.bottomAnchor)
        
        //BottomView
        lastView.bottomAnchor.constraint(equalTo: infoView.bottomAnchor, constant: Spacing.BOTTOM).isActive = true
    }
    
    private func collectionViewConstraints(){
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: collectionContentView.topAnchor, constant: ViewConstraints.SCROLL_VIEW_MARGIN),
            collectionView.leadingAnchor.constraint(equalTo: collectionContentView.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: collectionContentView.trailingAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: ViewConstraints.COLLECTION_VIEW_ITEM_HEIGHT),
            collectionView.widthAnchor.constraint(equalTo: collectionContentView.widthAnchor),
        ])
    }
    
    private func activityIndicatorViewConstraints(){
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            activityIndicatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            activityIndicatorView.topAnchor.constraint(equalTo: topAnchor),
            activityIndicatorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    //MARK: Helper Constraints
    private func addContentViewConstraints(view: UIView, height: CGFloat?, topAnchor: NSLayoutYAxisAnchor, topMargin: CGFloat? = nil){
        
        if let topMargin = topMargin {
            view.topAnchor.constraint(equalTo: topAnchor, constant: topMargin).isActive = true
        } else {
            view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            view.widthAnchor.constraint(equalTo: contentView.widthAnchor),
        ])
        if let height = height {
            view.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    private func addSubViewConstraints(view: UIView, contentView: UIView){
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: contentView.topAnchor),
            view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            view.widthAnchor.constraint(equalToConstant: ViewConstraints.CONTENT_WIDTH),
            view.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
    }
    
    private func addInfoSubView(infoView: UIView, titleView: UIView, valueView: UIView, topAnchor: NSLayoutYAxisAnchor) -> UIView{

        titleView.translatesAutoresizingMaskIntoConstraints = false
        valueView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            
            titleView.topAnchor.constraint(equalTo: topAnchor),
            titleView.heightAnchor.constraint(equalToConstant: ViewConstraints.TITLE_LABEL_HEIGHT),
            titleView.leadingAnchor.constraint(equalTo: infoView.leadingAnchor, constant: Spacing.LEFT),
            titleView.widthAnchor.constraint(equalToConstant: ViewConstraints.INFO_WIDTH),
            titleView.trailingAnchor.constraint(equalTo: valueView.leadingAnchor),
            
            valueView.topAnchor.constraint(equalTo: topAnchor),
            valueView.trailingAnchor.constraint(equalTo: infoView.trailingAnchor, constant: Spacing.RIGHT),
            valueView.heightAnchor.constraint(greaterThanOrEqualToConstant: ViewConstraints.TITLE_LABEL_HEIGHT)
        ])

        return valueView
    }
    
}
