//
//  EventTableViewCell.swift
//  AppTeste
//
//  Created by marcel.soares on 16/10/20.
//

import Foundation
import UIKit

class EventTableViewCell: UITableViewCell {

    // MARK: Subviews
    let titleLabel: UILabel = {
        let label = CustomLabel()
        label.font = Consts.TITLE_FONT
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = CustomLabel()
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = CustomLabel()
        label.textAlignment = .right
        return label
    }()
    
    var backView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.systemGray6
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.black.cgColor
        return view
    }()
    
    // MARK: Properties
    public var event : Event! {
        didSet {
            titleLabel.text = event.title
            dateLabel.text = event.date?.epochToDateStr()
            timeLabel.text = event.price?.epochToTimeStr()
        }
    }
    static let REUSE_IDENTIFIER = "cellId"

    // MARK: Initializers
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.white
        selectionStyle = .none
        configureCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = ""
        dateLabel.text = ""
        timeLabel.text = ""
    }

    // MARK: Private methods
    func configureCell(){
        addSubViews()
        createConstraints()
    }
    
    func addSubViews() {
        addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(dateLabel)
        backView.addSubview(timeLabel)
    }
    
    private func createConstraints() {
        
        backView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            backView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
            backView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: ViewConstraints.BOTTOM_SPACE),
            backView.topAnchor.constraint(equalTo: topAnchor, constant: ViewConstraints.TOP_SPACE)
        ])
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
            titleLabel.heightAnchor.constraint(equalToConstant: ViewConstraints.TITLE_LABEL_HEIGHT),
            titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: ViewConstraints.TOP_SPACE)
        ])
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
            titleLabel.heightAnchor.constraint(equalToConstant: ViewConstraints.TITLE_LABEL_HEIGHT),
            titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: ViewConstraints.TOP_SPACE)
        ])
        
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dateLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            dateLabel.heightAnchor.constraint(equalToConstant: ViewConstraints.SUB_TITLE_LABEL_HEIGHT),
            dateLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: ViewConstraints.TOP_SPACE),
            dateLabel.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: ViewConstraints.BOTTOM_SPACE)
        ])

        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            timeLabel.leadingAnchor.constraint(equalTo: dateLabel.trailingAnchor, constant: ViewConstraints.LEADING_SPACE),
            timeLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
            timeLabel.heightAnchor.constraint(equalToConstant: ViewConstraints.SUB_TITLE_LABEL_HEIGHT),
            timeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: ViewConstraints.TOP_SPACE),
            timeLabel.widthAnchor.constraint(equalTo: dateLabel.widthAnchor)
        ])

    }

}
