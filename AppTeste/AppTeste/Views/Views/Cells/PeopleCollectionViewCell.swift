//
//  PeopleCollectionViewCell.swift
//  AppTeste
//
//  Created by marcel.soares on 20/10/20.
//

import Foundation
import UIKit
import Nuke
import RxSwift


class PeopleCollectionViewCell: UICollectionViewCell {
    
    // MARK: Subviews
    var backView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.black.cgColor
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = CustomLabel()
        label.font = Consts.TITLE_FONT
        label.textAlignment = .center
        return label
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.transform = CGAffineTransform(scaleX: 1, y: 1)
        activityIndicator.tintColor = .gray
        activityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.1)
        return activityIndicator
    }()
    
    // MARK: Properties
    let disposeBag = DisposeBag()
    static let REUSE_IDENTIFIER = "cellId"
    
    public var people : Event.People! {
        didSet {
            titleLabel.text = people.name
            imageView.loadImageFromString(url: people.picture)
        }
    }
    
    
    // MARK: Initializers
    override init(frame: CGRect) {
        let frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        super.init(frame: frame)
        backgroundColor = .clear
        configureCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = ""
        imageView.image = nil
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true
    }

    func configureCell(){
        addSubViews()
        createConstraints()
        binds()
    }
    
    func binds() {
        imageView.rx.isEmpty.subscribe(onNext: { isEmpty in
            self.activityIndicatorView.isHidden = !isEmpty
            isEmpty ? self.activityIndicatorView.startAnimating() : self.activityIndicatorView.stopAnimating()
        }).disposed(by: self.disposeBag)
    }
    
    // MARK: Setup Layout
    func addSubViews() {
        addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(imageView)
        backView.addSubview(activityIndicatorView)
    }
    
    private func createConstraints() {
        
        backView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            backView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
            backView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: ViewConstraints.BOTTOM_SPACE),
            backView.topAnchor.constraint(equalTo: topAnchor, constant: ViewConstraints.TOP_SPACE)
        ])
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: ViewConstraints.TOP_SPACE),
            titleLabel.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
            titleLabel.heightAnchor.constraint(equalToConstant: ViewConstraints.TITLE_LABEL_HEIGHT),
        ])
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: ViewConstraints.TOP_SPACE),
            imageView.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: ViewConstraints.BOTTOM_SPACE),
            imageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: ViewConstraints.LEADING_SPACE),
            imageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: ViewConstraints.TRAILING_SPACE),
        ])
        
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicatorView.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            activityIndicatorView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            activityIndicatorView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
            activityIndicatorView.topAnchor.constraint(equalTo: imageView.topAnchor)
        ])

    }
    
}
