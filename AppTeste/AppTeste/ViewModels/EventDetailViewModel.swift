//
//  EventDetailViewModel.swift
//  AppTeste
//
//  Created by marcel.soares on 18/10/20.
//

import Foundation
import RxSwift
import RxCocoa

class EventDetailViewModel {
    
    // MARK: - Properties
    let event = PublishSubject<Event>()
    let people = PublishSubject<[Event.People]>()
    let error = PublishSubject<String>()
    
    let loading = PublishSubject<Bool>()
    let loadingImage = PublishSubject<Bool>()
    
    let disposeBag = DisposeBag()
    
    var shareText = ""
    
    //MARK: Event Detail
    func loadEventDetail(eventId: String) {

        loading.onNext(true)
        ServiceManager().loadEventDetail(eventId: eventId) { (errorMessage, eventResponse) in
            self.loading.onNext(false)
            
            if let eventResponse = eventResponse {
                
                //Format text to Share
                CustomFunctions().formatSharedMsg(event: eventResponse){ formatedString in
                    self.shareText = formatedString
                }
                
                self.event.onNext(eventResponse)
                self.event.onCompleted()
                self.people.onNext(eventResponse.people ?? [])
            } else {
                self.error.onNext(errorMessage ?? NSLocalizedString("Failed to event details", comment: ""))
            }
        }
    }
    
}
