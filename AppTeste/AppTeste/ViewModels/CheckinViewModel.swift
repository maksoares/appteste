//
//  CheckinViewModel.swift
//  AppTeste
//
//  Created by marcel.soares on 19/10/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa


class CheckinViewModel {
    
    // MARK: - Properties
    let nameError = PublishSubject<String>()
    let emailError = PublishSubject<String>()
    let checkin = PublishSubject<Bool>()
    let loading = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    

    //MARK: Events
    func checkin(eventId: String, name: String, email: String) {
        
        if name.count == 0 {
            self.nameError.onNext(NSLocalizedString("Invalid name!", comment: ""))
            return
        }
        
        if !CustomFunctions().isValidEmail(email: email) {
            self.emailError.onNext(NSLocalizedString("Invalid e-mail!", comment: ""))
            return
        }

        let parameters: [String: Any] = [
            "eventId": eventId,
            "name": name,
            "email": email
        ]
        
        loading.onNext(true)
        ServiceManager().checkin(parameters: parameters) { (messageError, checkinResponse) in
            self.loading.onNext(false)
            self.checkin.onNext(messageError == nil)
        }
    }
    
}
