//
//  EventsViewModel.swift
//  AppTeste
//
//  Created by marcel.soares on 14/10/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa


class EventsViewModel {
    
    // MARK: - Properties
    let events = PublishSubject<[Event]>()
    let error = PublishSubject<String>()
    let loading = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    

    //MARK: Events
    func loadEvents() {

        loading.onNext(true)
        ServiceManager().loadEvents() { (errorMessage, eventsResponse) in
            self.loading.onNext(false)
            
            if let eventsResponse = eventsResponse {
                self.events.onNext(eventsResponse)
            } else {
                self.error.onNext(errorMessage ?? NSLocalizedString("Failed to load events", comment: ""))
            }
        }
    }
    
}
