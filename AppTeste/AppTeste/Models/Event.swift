//
//  Event.swift
//  AppTeste
//
//  Created by marcel.soares on 14/10/20.
//

import Foundation


public struct Event: Decodable {
    
    var eventId: String?
    var date: Double?
    var description: String?
    var image: String?
    var longitude: Double?
    var latitude: Double?
    var price: Double?
    var title: String?
    var people: [People]?

    enum CodingKeys: String, CodingKey {

        case eventId = "id"
        case date = "date"
        case description = "description"
        case image = "image"
        case longitude = "longitude"
        case latitude = "latitude"
        case price = "price"
        case title = "title"
        case people = "people"
    }
 
    struct People: Codable {
        
        var picture: String?
        var name: String?
        var eventId: String?
        var peopleId: String?
        
        enum CodingKeys: String, CodingKey {
            case picture = "picture"
            case name = "name"
            case eventId = "eventId"
            case peopleId = "id"
        }
    }

}
